package jp.alhinc.hayashi_kenta.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {
	public static void main(String[] args) {
		HashMap<String, String> nameMap = new HashMap<String, String>();
		HashMap<String, Long> salesMap = new HashMap<String, Long >();

		BufferedReader br = null;

		try {
			File file = new File(args[0], "branch.lst");

			//支店定義ファイルが存在しない場合
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] arr = line.split(",",0);

				//数字のみ3桁固定
				if (!arr[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				//カンマ、改行を含まない文字
				if (arr.length >= 3) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				nameMap.put(arr[0],arr [1]);
				salesMap.put(arr[0],0L);
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
				}
			}
		}

		File file_2 = new File(args[0]);
		File salesfile[] = file_2.listFiles();
		for (int i = 0; i < salesfile.length; i++) {
			if (salesfile[i].getName().matches("^[0-9]{8}.rcd$")) {
				try {
					FileReader fr = new FileReader(salesfile[i]);
					br = new BufferedReader(fr);
					List<String> list = new ArrayList<String>();
					String line;
					while((line = br.readLine()) != null) {
						list.add(line);
					}

					// 支店に該当がなかった場合
					if (!nameMap.containsKey(list.get(0))) {
						System.out.println( salesfile[i].getName() + "の支店コードが不正です");
						return;
					}

					long added = salesMap.get(list.get(0));
					added += Long.parseLong(list.get(1));
					salesMap.put(list.get(0),added);

					// 合計金額が10桁を超えた場合
					String sum = Long.toString(added);
					if (10 < sum.length()) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}

					// 売り上げファイルの中身が3行以上ある場合
					if(list.size() >= 3){
						System.out.println( salesfile[i].getName() + "のフォーマットが不正です");
						return;
					}



				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				} finally {
					if(br != null) {
						try {
							br.close();
						} catch(IOException e) {
						}
					}
				}
			}

			try {
				File file_3 = new File(args[0], "branch.out");
				FileWriter fw = new FileWriter(file_3);
				BufferedWriter bw = new BufferedWriter(fw);

				for (String key : salesMap.keySet()) {
					bw.write(key + ",");
					bw.write(nameMap.get(key) + ",");
					String line = Long.toString(salesMap.get(key));
					bw.write(line);
					bw.newLine();
				}
				bw.close();

			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
					}
				}
			}
		}
	}
}
